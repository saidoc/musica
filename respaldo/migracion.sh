#!/bin/bash

echo "Inicia restauracion tabla panda"

echo "Inicia restauración imss"
mysqldump -uuserapp -pr3v3ng3$ --compact --add-drop-table --add-locks --extended-insert sistema asegurado archivo catsmg clienteclase movimientos rp domicilioRp rpdomicilio rpestado rpoperadoridse rpplazaopera rpriesgo rprlegal sucursales historial_mixtos incapacidad incapacidadvigente infonavit lotesdecarga lotesdocumento relaciones idse personal bancos estados trabajadores empresas clientes empresacte doctostrabajadores >> temp.sql
echo "Restauración imss terminada."

echo "Inicia restauración tablas apatodo"
mysqldump -uuserapp -pr3v3ng3$ --compact --add-drop-table --add-locks --extended-insert apatodo Empleados Expediente Oficinas plazamadre Usuarios >> temp.sql
echo "fin apatodo"

echo "Inicia restauración tablas godzilla"
mysqldump -uuserapp -pr3v3ng3$ --compact --add-drop-table --add-locks --extended-insert --no-create-info godzilla nom_godzilla nom_empresa nom_cliente nom_sucursal us_plaza >> temp.sql
echo "fin godzilla"

mysqldump -uuserapp -pr3v3ng3$ --compact --extended-insert --no-create-info panda estado >> temp.sql

mysql -uuserapp -pr3v3ng3$ sistema \
-e "SELECT DISTINCT n.idper \
    FROM master m \
   JOIN nominas n ON m.id=n.idnomina AND m.idcte=n.idcte AND m.idempresa=n.idempresa\
   WHERE n.al >= DATE_SUB(CURDATE(), INTERVAL 6 MONTH) AND m.tipocalculo='AS' AND m.proceso='N' AND m.eliminado='N'\
   ORDER BY n.al DESC" -B > personal_ias.txt

echo "Inicia la creacion del zip general"
tar cpzfv comprimido.tar.gz temp.sql personal_ias.txt
echo "fin zip general"

echo "Inicia limpiado sql"
rm *.sql
rm *.txt
echo "fin limpiado"
echo "sincronización avante siie"
rsync -avhP /var/www/html/respaldo/comprimido.tar.gz desarrollo4@192.168.1.212:/var/www/html/respaldo/
rm comprimido.tar.gz
